//
//  ViewController.swift
//  lec9
//
//  Created by Максим Бойко on 4/16/19.
//  Copyright © 2019 Максим Бойко. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

    var messageFrNew = "krokodil"
    
    
    
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func showText(_ sender: UISegmentedControl) {
        let prefix = sender.titleForSegment(at: sender.selectedSegmentIndex)!
        
        let wordToSend = prefix + " " + textField.text!
        
        performSegue(withIdentifier: "showText\(sender.selectedSegmentIndex + 2)", sender: wordToSend)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc2 = segue.destination as? VC2 {
            if let sendToVC2 = sender as? String {
            vc2.stringForDisplay = sendToVC2
        }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = messageFrNew
    }
     
    
}

